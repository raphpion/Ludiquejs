/*
 * spritesheet.js
 * Use this class to create an image element for a spritesheet
 *
 * By Raphaël Pion
 */

class SpriteSheet {
  constructor(source) {
    this.img = new Image()
    this.img.src = source
  }
}

export { SpriteSheet }
