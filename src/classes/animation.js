/*
 * animation.js
 * Use this class to create sprite animations that cycle through an array of sprites at a set interval
 *
 * By Raphaël Pion
 */

class Animation {
  constructor(spriteArray, interval) {
    this.sprites = spriteArray
    this.interval = interval
  }
  animate(object) {
    object.sprite = this.sprites[object.animationIndex]
    object.animationIndex++
    if (object.animationIndex >= this.sprites.length) object.animationIndex = 0
  }
  start(object) {
    // stop current animation
    if (object.animationIntervalId != null) object.animation.stop()

    // start animation interval
    object.animation = this
    object.animationInterval = setInterval(() => {
      this.animate(object)
    }, this.interval)

    object.animationIndex = 0
  }
  stop(object) {
    clearInterval(object.animationIntervalId)
  }
}

export { Animation }
