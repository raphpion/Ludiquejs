/*
 * sprite.js
 * Use this class to create sprites and eventually animate them
 *
 * By Raphaël Pion
 */

import { screen } from '../screen.js'

class Sprite {
  constructor(spritesheet, sx, sy, w, h) {
    this.spritesheet = spritesheet
    this.sx = sx
    this.sy = sy
    this.w = w
    this.h = h
  }
  draw(x = 0, y = 0) {
    screen.ctx.drawImage(this.spritesheet.img, this.sx, this.sy, this.w, this.h, x, y, this.w, this.h)
  }
}

export { Sprite }
